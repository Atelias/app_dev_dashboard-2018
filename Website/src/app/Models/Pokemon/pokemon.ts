export interface PokemonIdentity {
  id: number;
  french: string;
  english: string;
  deutsch: string;
}

export interface PokemonType {
  name: string;
}

export interface PokemonStats {
  pv: number;
  speed: number;
  attack: number;
  defense: number;
  speAttack: number;
  speDefense: number;
}

export interface PokemonSprite {
  front: string;
  back: string;
}

export interface Pokemon {
  identity: PokemonIdentity;
  types: PokemonType[];
  stats: PokemonStats;
  sprites: PokemonSprite;
}

