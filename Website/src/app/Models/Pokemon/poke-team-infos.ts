import { WidgetInfos }              from '../widget-infos';
import { HttpResponseUserWidgets }  from '../../Services/AuthServices/User/user.service';
import { Pokemon, PokemonIdentity } from './pokemon';
import { Vector }                   from '../Vector';

export class PokeTeamInfos extends WidgetInfos {
  private _pokemons: Pokemon[] = [];
  private _pokemonIds: PokemonIdentity[] = [];

  constructor() {
    super();
    this.size = new Vector(8, 6);
  }

  get pokelist(): PokemonIdentity[] {
    return this._pokemonIds;
  }

  set pokelist(value: PokemonIdentity[]) {
    this._pokemonIds = value;
  }

  public fromHttpWidgetModel(model: HttpResponseUserWidgets): PokeTeamInfos {
    const widget: WidgetInfos = super.fromHttpWidgetModel(model);

    this.uuid = widget.uuid;
    this.position = widget.position;
    this.title = widget.title;

    if (model.options) {
      const opt = JSON.parse(model.options);
      if (opt.pokemonIds) {
        this._pokemonIds = opt.pokemonIds;
      }
    }
    return this;
  }

  public getHttpWidgetModel(): HttpResponseUserWidgets {
    const data          = super.getHttpWidgetModel();
    const customOptions = {
      pokemonIds: this._pokemonIds,
    };

    if (data.options) {
      data.options = JSON.stringify(Object.assign(JSON.parse(data.options) || {}, customOptions));
    }

    data.widget = {
      id:          4,
      name:        undefined,
      description: undefined,
      options:     undefined,
      createdAt:   undefined,
      updatedAt:   undefined,
      service:     undefined,
    };
    return data;
  }
}
