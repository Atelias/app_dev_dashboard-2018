export class Query {
  _host: string;
  _route: string;

  constructor(host: string, route: string) {
    this._host  = host;
    this._route = route;
  }

  add(route: string): this {
    this._route += route;
    return this;
  }

  getUrl(): string {
    return this._host + this._route;
  }

  getRoute(): string {
    return this._route;
  }
}

export interface Url {
  hostname: string;
  port: number;
  protocol: string;
  entrypoint: string;
}

export class RequestBuilder {
  private _hostname: string   = 'localhost';
  private _port: number       = 3000;
  private _protocol: string   = 'http';
  private _entrypoint: string = '/';

  constructor(url: Url = null) {
    if (url) {
      if (url.entrypoint) {
        this._entrypoint = url.entrypoint;
      }
      if (url.port) {
        this._port = url.port;
      }
      if (url.protocol) {
        this._protocol = url.protocol;
      }
      if (url.hostname) {
        this._hostname = url.hostname;
      }
    }
  }

  get hostname(): string {
    return this._hostname;
  }

  set hostname(value: string) {
    this._hostname = value;
  }

  get port(): number {
    return this._port;
  }

  set port(value: number) {
    this._port = value;
  }

  get protocol(): string {
    return this._protocol;
  }

  set protocol(value: string) {
    this._protocol = value;
  }

  get entrypoint(): string {
    return this._entrypoint;
  }

  set entrypoint(value: string) {
    this._entrypoint = value;
  }

  getQuery() {
    let host: string  = '';
    let route: string = '';

    host += this.protocol + '://' + this._hostname;
    if (this.port !== 80) {
      host += ':' + this.port;
    }
    route = this._entrypoint;

    return new Query(host, route);
  }
}
