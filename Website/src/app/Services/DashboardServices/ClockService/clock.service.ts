import { Injectable } from '@angular/core';
// @ts-ignore
import Timer = NodeJS.Timer;
import { interval }   from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClockService {
  private _interval: Timer;
  private _now: Date = new Date();

  constructor() {
    this._interval = interval(500).subscribe((value => {
      this._now = new Date();
    }))
  }

  get now(): Date {
    return this._now;
  }
}
