import { Injectable }               from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { User, UUID }               from '../../../Models/User';
import { WidgetService }            from '../../WidgetsService/widget.service';

export interface HttpResponseService {
  id: number;
  name: string;
  description: string;
  createdAt: string;
  updatedAt: string;
  hostApi: null;
}

export interface HttpResponseWidget {
  id: number;
  name: string;
  description: string;
  options: string;
  createdAt: Date;
  updatedAt: Date;
  service: HttpResponseService;
}

export interface HttpResponseUserWidgets {
  createdAt: Date;
  updatedAt: Date;
  uuid: string;
  widget: HttpResponseWidget;
  options: string;
  positionX: number;
  positionY: number;
}

export interface HttpResponseMe {
  uuid: string;
  username: string;
  mail: string;
  createdAt: Date;
  updatedAt: Date;
  widgets: HttpResponseUserWidgets[];
}

@Injectable({
              providedIn: 'root',
            })
export class UserService {

  private _user: User = new User();

  constructor(private http: HttpClient,
              private _wm: WidgetService) {

  }

  public refresh() {
    return this.http
               .get<HttpResponseMe>('http://localhost:3000/me', {observe: 'response'})
               .subscribe((resp: HttpResponse<HttpResponseMe>) => {
                 this._user.mail     = resp.body.mail;
                 this._user.username = resp.body.username;
                 this._user.uuid     = resp.body.uuid;
                 this._wm.refresh(resp.body);
               });
  }

  get user(): User {
    return this._user;
  }

  get uuid(): UUID {
    return this.user.uuid;
  }

  get username(): string {
    return this.user.username;
  }

  get mail(): string {
    return this.user.mail;
  }
}
