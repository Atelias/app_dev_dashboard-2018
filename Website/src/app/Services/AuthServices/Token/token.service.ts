import { Injectable } from '@angular/core';
import { Token }      from '../Login/login.service';

function numberToDate(...arg: any[]): (string, any) => any {
  return (key, value) => {
    for (const a in arg) {
      if (key === a) {
        return new Date(value);
      }
    }
    return value;
  };
}

@Injectable({
              providedIn: 'root',
            })
export class TokenService {

  constructor() {
  }

  public getToken(): Token {
    const lSToken: string = localStorage.getItem('authToken');
    const lSTokenExpire: string = localStorage.getItem('token-expire');

    if (!lSToken || !lSTokenExpire) {
      return null;
    }

    return {
      createdAt: new Date(),
      expiresAt: new Date(lSTokenExpire),
      token:     lSToken,
    };
  }

  public setToken(token: Token) {
    token.createdAt = new Date(token.createdAt);
    token.expiresAt = new Date(token.expiresAt);
    localStorage.setItem('authToken', '' + token.token);
    localStorage.setItem('token-expire', '' + token.expiresAt.getTime());
  }

  public clearToken(): void {
    localStorage.removeItem('authToken');
    localStorage.removeItem('token-expire');
  }

  public isTokenValid(): boolean {
    const tkn: Token = this.getToken();

    if (!tkn || !tkn.expiresAt || tkn.expiresAt.getTime() >= (new Date()).getTime()) {
      this.clearToken();
      return false;
    }
    return true;
  }

  public get token(): string {
    return this.getToken().token;
  }
}
