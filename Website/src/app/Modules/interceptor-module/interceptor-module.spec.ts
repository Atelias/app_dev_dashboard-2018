import { InterceptorModuleModule } from './interceptor-module';

describe('InterceptorModuleModule', () => {
  let interceptorModuleModule: InterceptorModuleModule;

  beforeEach(() => {
    interceptorModuleModule = new InterceptorModuleModule();
  });

  it('should create an instance', () => {
    expect(interceptorModuleModule).toBeTruthy();
  });
});
