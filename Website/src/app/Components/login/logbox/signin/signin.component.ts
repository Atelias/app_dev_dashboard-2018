import { Component, Input, OnInit }           from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginService }                       from '../../../../Services/AuthServices/Login/login.service';
import { MatDialogRef }                       from '@angular/material';
import { LogboxComponent }                    from '../logbox.component';
import { UserService }                        from '../../../../Services/AuthServices/User/user.service';

@Component({
             selector:    'app-signin',
             templateUrl: './signin.component.html',
             styleUrls:   ['./signin.component.css'],
           })
export class SigninComponent implements OnInit {

  public _form: FormGroup = new FormGroup({
                                            'username': new FormControl('', [Validators.required]),
                                            'password': new FormControl('', [Validators.required]),
                                          });

  constructor(private _loginManager: LoginService,
              private _userService: UserService) {
  }

  @Input('dialogRef') dialogRef: MatDialogRef<LogboxComponent>;

  ngOnInit() {
  }

  loggUser() {
    this._loginManager.signin(this._form.get('username').value, this._form.get('password').value).add(() => {
      this._userService.refresh().add(() => {
        this.dialogRef.close();
      });
    });
  }
}
