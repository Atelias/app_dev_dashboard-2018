import { Component, OnInit }       from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { LogboxComponent }         from './logbox/logbox.component';
import { LoginService }            from '../../Services/AuthServices/Login/login.service';
import { UserService }             from '../../Services/AuthServices/User/user.service';

@Component({
             selector:    'app-login',
             templateUrl: './login.component.html',
             styleUrls:   ['./login.component.css'],
           })
export class LoginComponent implements OnInit {

  animal: string;
  name: string;

  constructor(public dialog: MatDialog,
              private loginService: LoginService,
              private userService: UserService) {
  }

  ngOnInit() {
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(LogboxComponent, {
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  logout(): void {
    this.loginService.logout();
  }

  get logged() {
    return this.loginService.logged;
  }

  get username(): string {
    return this.userService.username;
  }

}
