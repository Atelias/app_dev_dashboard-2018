import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokeTeamConfigComponent } from './poke-team-config.component';

describe('PokeTeamConfigComponent', () => {
  let component: PokeTeamConfigComponent;
  let fixture: ComponentFixture<PokeTeamConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokeTeamConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokeTeamConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
