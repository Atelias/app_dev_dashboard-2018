import { Component, Input, OnInit } from '@angular/core';
import { PokemonService }          from '../../../../../Services/DashboardServices/PokemonService/pokemon.service';
import { WidgetService }           from '../../../../../Services/WidgetsService/widget.service';
import { PokeTeamInfos }           from '../../../../../Models/Pokemon/poke-team-infos';
// @ts-ignore
import Timer = NodeJS.Timer;
import { Vector }                  from '../../../../../Models/Vector';
import { PokeTeamConfigComponent } from './poke-team-config/poke-team-config.component';
import { PokemonIdentity }         from '../../../../../Models/Pokemon/pokemon';

@Component({
             selector:    'app-poke-team',
             templateUrl: './poke-team.component.html',
             styleUrls:   ['./poke-team.component.css'],
           })
export class PokeTeamComponent implements OnInit {

  @Input('options') _options: PokeTeamInfos;
  private _delay: Timer = null;

  constructor(private _pokemonService: PokemonService, private _ws: WidgetService) {
  }

  ngOnInit() {
  }

  get configModal(): typeof PokeTeamConfigComponent {
    return PokeTeamConfigComponent;
  }

  get position(): Vector {
    return this._options.position;
  }

  get size(): Vector {
    return this._options.size;
  }

  get title(): string {
    return this._options.title;
  }

  get widgetInfos(): PokeTeamInfos {
    return this._options;
  }

  get pokelist(): PokemonIdentity[] {
    return this._options.pokelist;
  }

}
